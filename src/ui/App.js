import React, { PureComponent } from 'react'
import AceEditor from 'react-ace'
import styled from 'styled-components'

import 'ace-builds/src-noconflict/mode-css'
import 'ace-builds/src-noconflict/theme-solarized_dark'
import 'ace-builds/src-noconflict/theme-monokai'

const AppContainer = styled.div`
  display: flex;
`

const FlexContainer = styled.div`
  /* display: flex;
  width: 50vw;
  height: 100vh; */
  ${({ css }) => css}
`

const FlexItems = styled.div`
  width: 50px;
  height: 50px;
  background-color: ${({ backgroundColor }) => backgroundColor ? backgroundColor : 'gray'};
`

class App extends PureComponent {
  constructor(props) {
    super(props)
    this.initialState = {
      flexParentCss: `
        display: flex;
        width: 50vw;
        height: 100vh;
      `
    }
    this.state = this.initialState
    this._onChange = this._onChange.bind(this)
  }
  _onChange(newValue) {
    this.setState({ flexParentCss: newValue })
  }
  render() {
    const { flexParentCss } = this.state
    return (
      <AppContainer>
        <FlexContainer css={flexParentCss}>
          <FlexItems backgroundColor="yellow" />
          <FlexItems backgroundColor="red" />
          <FlexItems backgroundColor="blue" />
        </FlexContainer>
        <AceEditor
          mode="css"
          theme="monokai"
          value={flexParentCss}
          width="50vw"
          height="100vh"
          onChange={this._onChange}
        />
      </AppContainer>
    )
  }
}

export default App
